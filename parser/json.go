package parser

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

// A struct to define the main root level structure of the config json
type JsonStructure struct {
	RootNode []FileFolder `json:"directory"`
}

// A struct to define the file and folder structure of the config json
type FileFolder struct {
	Name     string        `json:"name"`
	Type     string        `json:"type"`
	Action   *Action       `json:"action"`
	Children *[]FileFolder `json:"children"`
}

// A struct to define the actions on the file/folder of the config json
type Action struct {
	Type string `json:"type"`
	Name string `json:"name"`
	From string `json:"from"`
}

// A function that parses the config json
func parseJson(filePath string) {
	fileObj, err := ioutil.ReadFile(filePath)
	if err == nil {
		var jsonDir JsonStructure
		jsonErr := json.Unmarshal(fileObj, &jsonDir)
		if jsonErr == nil {
			log.Println("the name: ", jsonDir.RootNode[0].Name)
		} else {
			log.Panic(jsonErr)
		}

	} else {
		log.Panic(err)
	}

}
