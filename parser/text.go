package parser

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func createAction(folderMapFile string) {
	lines := readFolderMap(folderMapFile)
	for _, each_ln := range lines {
		createFileFolder(each_ln)
	}
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readFolderMap(folderMapFile string) []string {
	file, err := os.Open(folderMapFile)
	check(err)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var text []string
	for scanner.Scan() {
		text = append(text, scanner.Text())
	}
	return text
}

func createEmptyFile(name string) {
	d := []byte("")
	check(os.WriteFile(name, d, 0644))
}

func isDirectory(name string) bool {
	return strings.HasSuffix(name, "/")
}

func createFileFolder(name string) {
	if isDirectory(name) {
		fmt.Printf("Creating directory: %v\n", name)
		err := os.MkdirAll(name, 0755)
		check(err)
	} else {
		fmt.Printf("Creating file: %v\n", name)
		dirName, _ := filepath.Split(name)
		err := os.MkdirAll(dirName, 0755)
		check(err)
		createEmptyFile(name)
	}

}
