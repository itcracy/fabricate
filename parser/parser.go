package parser

import (
	"fmt"
	"path/filepath"
)

// the main function that will receive the file path from args
// this will call the respective file parse functions based on the input config file type
func Parse(argPath string) {
	fileExt := filepath.Ext(argPath)
	switch fileExt {
	case ".txt", "":
		createAction(argPath)
	case ".json":
		fmt.Println("json file found")
		parseJson(argPath)
	case ".yml", ".yaml":
		fmt.Println("yaml file found")
	case ".toml":
		fmt.Println("toml file found")
	}

}
