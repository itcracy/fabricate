package cmd

import (
	"gitlab.com/itcracy/fabricate/parser"

	"github.com/spf13/cobra"
)

// applyCmd represents the apply command
var applyCmd = &cobra.Command{
	Use:   "apply [config file]",
	Short: "Apply folder structure from file",
	Long: `Apply folder structure from file.
For example:

File with lines:

foo/bar/baz.txt
foobaz/

Will apply to current directory creating all folders and files above.
`,
	Args: cobra.MinimumNArgs(1),
	Run: func(_ *cobra.Command, args []string) {
		folderMapFile := args[0]
		parser.Parse(folderMapFile)
	},
}

func init() {
	rootCmd.AddCommand(applyCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// applyCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// applyCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
