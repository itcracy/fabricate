.DEFAULT_GOAL := build
PACKAGE_NAME := fabricate

# For run commmands use the rest as arguments and turn them into do-nothing targets
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),run))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

build: # build executable
	go build -o ${PACKAGE_NAME}

install: # build and install to gopath
	go install

run: # compile & run program
	go run main.go $(RUN_ARGS)

clean: # remove build files
	rm -f fabricate
	rm -f ${GOPATH}/bin/${PACKAGE_NAME}

format: # format code
	gofmt -w .

quality: # check quality
	go fmt $(go list ./... | grep -v /vendor/)
	go vet $(go list ./... | grep -v /vendor/)

test: # run test
	go test -race $(go list ./... | grep -v /vendor/)

qa: quality test
